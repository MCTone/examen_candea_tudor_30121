import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Candea Tudor
 * @version 1
 */

public class Subiectul2 {
    public static void main(String[] args) {
        new Win();
    }
}

class Win extends JFrame {
    JTextArea textArea;
    JButton button;

    //declarare nume de fisier
    String FILE_NAME = "abcd";
    String destinationFile = FILE_NAME + ".txt";

    public Win() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 400);
        setLayout(null);

        button = new JButton("Print to file");
        button.setBounds(10, 30, 180, 20);
        button.addActionListener(e -> {
            printfile();
        });

        textArea = new JTextArea();
        textArea.setBounds(10, 105, 150, 100);

        add(textArea);
        add(button);
        setVisible(true);
    }

    // metoda pt creare fisier
    public void printfile() {

        try (FileWriter fileWriter =
                     new FileWriter("subiectul2_file/" + destinationFile)) {
            String d = textArea.getText();
            fileWriter.write(d + "\n");
            fileWriter.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}