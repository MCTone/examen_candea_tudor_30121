
/**
 * @author Candea Tudor
 * @version 1
 */

public class Subiectul1 {
}

class G {
    public void i(){
    }
}//.class

//B mosteneste G si intre B si E e de asociere
class B extends G{
    private long t;
    private E ee;

    B(){
        B b= new B();
        b.i();
    }

    public void x(){
        ee.metG((int)t);
        ee.doing();
    }

}//.class

//D e in relatie de dependenta cu B
class D{
    public void doD(B b){
        b.x();
        b.i();
    }
}//.class



//interfata
interface I{
    public void doing();
}

//clasa E implementeaza intefata I
class E implements I{
    public void metG(int i){
    }

    @Override
    public void doing() {
    }
}//.class


//F e in relatie de compozitie cu E
class F{

    public void metA(){
        new E();
    }
}//.class


//H e in relatie de agregare cu E
class H{
    public E d;

    public void doH(E dd){
        d=dd;
    }
}//.class
